create extension if not exists "uuid-ossp";
create extension if not exists citext;

create or replace function sid(prefix text) returns text
as $$ select $1 || '.' || replace(uuid_generate_v4()::text,'-','') $$
language sql;

create table pub78 (
    ein text,
    name text,
    city text,
    state text,
    country text,
    status_code text,
    "created" timestamp not null default CURRENT_TIMESTAMP,
    primary key (ein)
);

create table revocation (
    ein        text,
    name       text,
    other_name text,
    primary key (ein)
)
