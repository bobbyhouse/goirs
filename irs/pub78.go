package irs

import "strings"

type Pub78 struct {
	tableName  struct{} `pg:"pub78"`
	EIN        string
	Name       string
	City       string
	State      string
	Country    string
	StatusCode string
}

func Pub78DataURI() string {
	uri := "https://apps.irs.gov/pub/epostcard/"
	return uri
}

func Pub78Filename() string {
	return "data-download-pub78"
}

func Pub78Parse(pub78Model *Pub78, text string) bool {
	columns := map[string]int{
		"EIN":        0,
		"Name":       1,
		"City":       2,
		"State":      3,
		"Country":    4,
		"StatusCode": 5,
	}

	parse := Parse{
		Columns:   columns,
		Separator: "|",
		Length:    6,
	}

	attributes := strings.Split(text, parse.Separator)

	if len(attributes) < parse.Length {
		return true
	}

	pub78Model.EIN = attributes[columns["EIN"]]
	pub78Model.Name = attributes[columns["Name"]]
	pub78Model.City = attributes[columns["City"]]
	pub78Model.State = attributes[columns["State"]]
	pub78Model.Country = attributes[columns["Country"]]
	pub78Model.StatusCode = attributes[columns["StatusCode"]]

	return false
}
