package irs

import "strings"

type Revocation struct {
	tableName struct{} `pg:"revocation"`
	EIN       string
	Name      string
	OtherName string
}

func RevocationDataURI() string {
	uri := "https://apps.irs.gov/pub/epostcard/"
	return uri
}

func RevocationFilename() string {
	return "data-download-revocation"
}

func RevocationExtension() string {
	return "zip"
}

func RevocationParse(revocationModel *Revocation, text string) bool {
	columns := map[string]int{
		"EIN":       0,
		"Name":      1,
		"OtherName": 2,
	}

	parse := Parse{
		Columns:   columns,
		Separator: "|",
		Length:    11,
	}

	attributes := strings.Split(text, parse.Separator)

	if len(attributes) < parse.Length {
		return true
	}

	revocationModel.EIN = attributes[columns["EIN"]]
	revocationModel.Name = attributes[columns["Name"]]
	revocationModel.OtherName = attributes[columns["OtherName"]]

	return false
}
