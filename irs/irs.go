package irs

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

type IRSDataSearch struct {
	Name      string
	URI       string
	Search    string
	Filename  string
	Extension string
	Filepath  string
	Metadata  Meta
}

type Meta struct {
	Type         string    `json:"type"`
	Filename     string    `json:"filename"`
	MD5          string    `json:"md5"`
	RecordedDate time.Time `json:"recorded_date"`
	RunDate      time.Time `json:"run_date"`
	Success      bool
}

type Parse struct {
	Columns   map[string]int
	Separator string
	Length    int
}

func httpRequest(uri string, metadata *Meta) (bool, error) {
	client := http.Client{}
	resp, err := client.Get(uri)

	if err != nil {
		return false, err
	}

	buf := new(bytes.Buffer)
	if _, err := io.Copy(buf, resp.Body); err != nil {
		log.Fatal(err)
	}

	md5Sum := fmt.Sprintf("%x", md5.Sum(buf.Bytes()))
	if md5Sum == metadata.MD5 {
		return false, err
	}

	defer resp.Body.Close()

	metadata.MD5 = md5Sum
	filename := fmt.Sprintf("%s.%s", metadata.Filename, "zip")
	file, _ := os.Create(filename)
	io.Copy(file, buf)

	defer file.Close()
	return true, err
}

func fetchFile(search *IRSDataSearch, metadata *Meta) {
	uri := search.URI
	filename := search.Filename
	extension := search.Extension

	fetchURI := fmt.Sprintf("%s/%s.%s", uri, filename, extension)

	metadata.Type = search.Name
	writeMetadata(metadata)

	status, _ := httpRequest(fetchURI, metadata)
	metadata.Success = status
	writeMetadata(metadata)
}

func writeMetadata(metadata *Meta) {
	metaFileName := fmt.Sprintf("_%s", metadata.Filename)

	fMetaOut, err := os.Create(metaFileName)
	if err != nil {
		log.Fatal(err)
	}

	enc := json.NewEncoder(fMetaOut)
	err = enc.Encode(metadata)
	if err != nil {
		log.Fatal(err)
	}
	fMetaOut.Close()
}

func readMetadata(filename string) Meta {
	metaFileName := fmt.Sprintf("_%s", filename)

	fMeta, err := os.Open(metaFileName)

	metaIn := Meta{
		Filename: filename,
		Success:  false,
	}

	if err == nil {
		decoded := json.NewDecoder(fMeta)
		_ = decoded.Decode(&metaIn)
	}

	fMeta.Close()
	return metaIn
}

func Download(search *IRSDataSearch) Meta {
	filename := search.Filename
	now := time.Now().UTC()
	metaIn := readMetadata(filename)

	metaOut := Meta{Filename: metaIn.Filename,
		MD5:          metaIn.MD5,
		RecordedDate: metaIn.RecordedDate,
		RunDate:      now,
		Success:      false,
	}

	writeMetadata(&metaOut)

	fetchFile(search, &metaOut)
	return metaOut
}
