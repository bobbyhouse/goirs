package main

import (
	"archive/zip"
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"

	"funraise.io/goirs/irs"
	"github.com/go-pg/pg"
)

type Repo struct {
	db *pg.DB
}

func NewRepo(db *pg.DB) Repo {
	return Repo{db}
}

func InsertPub78(repo Repo, pub78 irs.Pub78) (err error) {
	_, err = repo.db.Model(&pub78).Insert()

	if err != nil {
		panic(err)
	}
	return err
}

func InsertRevocation(repo Repo, revocation irs.Revocation) (err error) {
	_, err = repo.db.Model(&revocation).Insert()

	if err != nil {
		panic(err)
	}
	return err
}

func InsertZipFile(repo Repo, metadata irs.Meta) {
	filepath := fmt.Sprintf("%s.zip", metadata.Filename)
	zipFile, err := zip.OpenReader(filepath)

	pub78Model := irs.Pub78{}
	revocationModel := irs.Revocation{}

	if err != nil {
		log.Fatal(err)
	}

	defer zipFile.Close()

	re := regexp.MustCompile(` +\r?\n +`)

	for _, f := range zipFile.File {
		file, err := f.Open()

		if err != nil {
			log.Fatal(err)
		}

		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			text := scanner.Text()
			text = re.ReplaceAllString(text, "")

			var skip bool
			if metadata.Type == "pub78" {
				skip = irs.Pub78Parse(&pub78Model, text)

				if skip {
					continue
				}

				InsertPub78(repo, pub78Model)

			} else if metadata.Type == "revocation" {
				skip = irs.RevocationParse(&revocationModel, text)

				if skip {
					continue
				}

				fmt.Println(revocationModel.Name)
				fmt.Println(revocationModel.EIN)
				fmt.Println(revocationModel.OtherName)

				InsertRevocation(repo, revocationModel)
			}

		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}
}

type Parse func(*irs.Pub78, string) bool

type Foo struct {
	Model   irs.Pub78
	Extract Parse
}

func main() {
	log.SetPrefix("goirs: ")
	log.SetFlags(0)

	db := pg.Connect(&pg.Options{
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASS"),
		Addr:     os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT"),
		Database: os.Getenv("DB_NAME"),
	})

	pub78Repo := NewRepo(db)
	revocationsRepo := NewRepo(db)

	pub78Filename := irs.Pub78Filename()
	pub78Extension := "zip"
	pub78FilePath := fmt.Sprintf("%s.%s", pub78Filename, pub78Extension)

	pub78 := irs.IRSDataSearch{
		Name:      "pub78",
		URI:       irs.Pub78DataURI(),
		Filename:  pub78Filename,
		Extension: pub78Extension,
		Filepath:  pub78FilePath,
	}

	metadata := irs.Download(&pub78)

	if metadata.Success {
		fmt.Println("Insert Pub78")
		InsertZipFile(pub78Repo, metadata)
	}

	revocationFilename := irs.RevocationFilename()
	revocationExtension := irs.RevocationExtension()
	revocationFilepath := fmt.Sprintf("%s.%s", revocationFilename, revocationExtension)

	revocation := irs.IRSDataSearch{
		Name:      "revocation",
		URI:       irs.RevocationDataURI(),
		Filename:  revocationFilename,
		Extension: revocationExtension,
		Filepath:  revocationFilepath,
	}

	metadata = irs.Download(&revocation)

	if metadata.Success {
		fmt.Println("Insert Revocation")
		InsertZipFile(revocationsRepo, metadata)
	}
}
